from django.contrib import admin

# Register your models here.
from .models import Activity, Person
admin.site.register(Activity)
admin.site.register(Person)