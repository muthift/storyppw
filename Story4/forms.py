from django import forms
from .models import Activity
import datetime

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['Day', 'Date', 'Time', 'Name', 'Place', 'Category']
        