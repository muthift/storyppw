from django.db import models 
from django.utils import timezone
from datetime import datetime, date
class Activity(models.Model): 
    Day = models.CharField(max_length=8)
    Date = models.DateField(default=datetime.now())
    Time = models.TimeField(default=timezone.now())
    Name = models.CharField(max_length=30)
    Place = models.CharField(max_length=30)
    Category = models.CharField(max_length=30)

class Person(models.Model):
    author = models.ForeignKey(Activity, on_delete = models.CASCADE)
    content = models.CharField(max_length=100)
    published_date = models.DateTimeField(default=timezone.now)

   

