from django.urls import path
from . import views

urlpatterns = [
    path('', views.Story41, name='Story41'),
    path('home/', views.Story41, name='Story41'),
    path('about/', views.Story42, name='Story42'),
    path('registration/', views.Story43, name='Story43'), 
    path('form/', views.form, name='form'),  
    path('delete/', views.delete, name='delete'),    
]