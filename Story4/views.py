from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse
from .forms import ActivityForm
from .models import Activity    
# Create your views here.
def Story41(request):
    return render(request, 'Story41.html', {})

def Story42(request):
    return render(request, 'Story42.html', {})

def Story43(request):
    return render(request, 'Story43.html', {})

def form(request):
    all_schedules = Activity.objects.all()
    if request.method == 'POST':
        activity_form = ActivityForm(request.POST)
        if activity_form.is_valid():
            activity_form.save()
            return HttpResponseRedirect(reverse('form'))
    else:
        activity_form = ActivityForm()
    return render(request, 'form.html', {'form': activity_form, 'schedules': all_schedules})

def delete(request):
    Activity.objects.all().delete()
    return HttpResponseRedirect(reverse('form'))